# Setup configs
cp ./config.js ./gekko/config.js &&
cp ./ecosystem.config.js ./gekko/ecosystem.config.js &&
# Setup
cp -r ./config/strategies/* ./gekko/config/strategies/ &&
# Setup
cp -r ./strategies/* ./gekko/strategies/