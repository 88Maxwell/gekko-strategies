rm -rf gekko &&
git clone git://github.com/askmike/gekko.git -b stable gekko &&

echo "" &&
echo "==================================================================" &&
echo "=======         Gekko successfuly cloned!!!                =======" &&
echo "==================================================================" &&
echo "" &&

cd gekko &&
npm install &&
cd exchange &&
npm install &&
cd ../.. &&

echo "" &&
echo "==================================================================" &&
echo "=======         Gekko successfuly installed!!!             =======" &&
echo "==================================================================" &&
echo "" &&

bash install_strategies.sh &&

echo "" &&
echo "==================================================================" &&
echo "=======         Strategies successfuly installed!!!        =======" &&
echo "==================================================================" &&
echo ""