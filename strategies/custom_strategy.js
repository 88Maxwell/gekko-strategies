var strategy = {};

// prepare everything our strategy needs
strategy.init = function () {
  this.upper_limit = this.settings.thresholds.upper_limit / 100;
  this.lower_limit = this.settings.thresholds.lower_limit / 100;
  this.stopLossPrice = 0;
  this.trend = { boughtOut: false, direction: "" };
  this.buyCond = false;
}

// what happens on every new candle?
strategy.update = function (candle) {
  var { close, open } = candle;
  var { boughtOut } = this.trend;
  var buyCond = close / open > 1 + this.upper_limit;

  // SET STOP LOSS
  if (this.trend.boughtOut && this.trend.direction === "hight") {
    this.stopLossPrice = candle.open * (1 - this.lower_limit);
  }

  this.trend.direction = close / open > 1 ? "hight" : "low";

  // SELL
  if (boughtOut && close < this.stopLossPrice) {
    this.trend.boughtOut = false;
    this.stopLossPrice = 0;
    return this.advice("short");
  }

  // BUY
  if (!boughtOut && buyCond) {
    this.trend.boughtOut = true;
    return this.advice("long");
  }

  return false;
}

module.exports = strategy;
