var chai = require('chai');
var expect = chai.expect;

function generateSettings(upper_limit, lower_limit) {
  return {
    thresholds: {
      upper_limit: upper_limit,
      lower_limit: lower_limit
    }
  }
}


function offsetCandle(candle, offset) {
  return {
    "open": candle.open + offset,
    "close": candle.close + offset,
  }
}


var CANDLES = {
  positiveBig: { "open": 100, "close": 103 },
  positiveSmall: { "open": 100, "close": 101 },
  negativeBig: { "open": 100, "close": 97 },
  negativeSmall: { "open": 100, "close": 99 },
  neutralSmall: { "open": 100, "close": 100 }
}


var strategy;

describe('strategies/custom_strategy', function() {
  beforeEach(function() {
    var path = __dirname + '/../../strategies/custom_strategy';

    delete require.cache[require.resolve(path)];

    strategy = require(path);
    strategy.settings = generateSettings(2, 2);
    strategy.advice = function(result) {
      return result;
    }
    strategy.init();
  });

  describe('if we have coins', function() {
    beforeEach(function() {
      strategy.update(CANDLES.positiveBig);
    });

    it('POSITIVE: should sell if closed candle`s price less than stop loss', function() {
      var result = strategy.update(offsetCandle(CANDLES.negativeBig, 3));

      expect(result).to.equal("short");
    })

    it('NEGATIVE: shouldn`t do anything if closed candle`s price greater than stop loss', function() {
      var result = strategy.update(offsetCandle(CANDLES.positiveBig, 3));

      expect(result).to.be.false;
    })

    it('POSITIVE: shouldn`t do anything if trend direction is high', function() {
      var result = strategy.update(offsetCandle(CANDLES.negativeBig, 3));

      expect(result).to.equal("short");
    })
  })

  describe('if we have no coins', function() {
    beforeEach(function() {
      strategy.update(CANDLES.neutralSmall);
    });

    it('POSITIVE: should buy if closed candle`s price greater than upper limit', function() {
      var result = strategy.update(CANDLES.positiveBig);

      expect(result).to.equal("long");
    })

    it('NEGATIVE: should do anything if closed candle`s price less than upper limit', function() {
      var result = strategy.update(CANDLES.positiveSmall);

      expect(result).to.be.false;
    })

    it('NEGATIVE: should do anything if trend direction is low', function() {
      var result = strategy.update(CANDLES.negativeBig);

      expect(result).to.be.false;
    })
  })
});
